#! /usr/bin/python3

import click
import random
import asyncio
from threading import Thread, Event
from nio import AsyncClient, AsyncClientConfig, RoomMessage, RoomMessageText, ReceiptEvent, MatrixRoom, InviteEvent, UnknownEvent
from importlib import import_module

LEARN_PREFIX = 'learn'
MAX_MSG_LEN = 100

@click.command()
@click.argument('server')
@click.argument('user_id')
@click.argument('token')
@click.argument('device_id')
@click.argument('store_path')
@click.argument('messages')
@click.option('--plugin', default=None)
@click.option('--extra-triggers', default=None)
def main(server: str, user_id: str, token: str, device_id: str, store_path: str, messages: str, plugin: str, extra_triggers: str):
    plugin_method = None
    if plugin:
        module, method = plugin.split('.', 1)
        plugin_module = import_module(module)
        plugin_method = getattr(plugin_module, method)

    config = AsyncClientConfig(store_sync_tokens=True, encryption_enabled=True)
    client = AsyncClient(server, user_id, config=config, store_path=store_path)
    client.restore_login(user_id=user_id, device_id=device_id, access_token=token)

    extra_triggers = [e.lower() for e in extra_triggers.split(',') if e] if extra_triggers else []
    bot = Bot(messages, client, user_id, device_id, store_path, plugin_method, extra_triggers)
    asyncio.get_event_loop().run_until_complete(bot.run())
    return bot.run()

def load_messages(file: str) -> list:
    with open(file) as fd:
        return [e.replace('\n', '') for e in fd.readlines() if e.strip()]

class Bot:
    def __init__(self, db: str, client: str, user_id: str, device_id: str, store_path: str, plugin, extra_triggers: list):
        self.plugin = plugin
        self.client = client
        self.db = db
        self.message_queue = []
        self.user_id = user_id
        self.user_name = user_id.split(':')[0].replace('@', '')
        self.client.add_event_callback(self.on_message, RoomMessageText)
        self.client.add_event_callback(self.on_message_react, UnknownEvent)
        self.client.add_event_callback(self.on_room_invite, InviteEvent)
        self.extra_triggers = extra_triggers

    async def on_message(self, room, event: RoomMessageText):
        if self.client.should_upload_keys:
            self.client.keys_upload()

        thread = Thread(target=self.on_message_impl, args=(asyncio.get_event_loop(), room,event,))
        thread.daemon = True
        thread.start()

    def call_plugin(self, loop, event, room):
        def callback(routine):
            task = loop.create_task(routine(self.client))
            event = Event()
            task.add_done_callback(lambda e: event.set())
            event.wait()

            return task.result()

        return self.plugin and self.plugin(event, room, callback)

    def on_message_impl(self, loop, room, event: RoomMessageText):
        if event.sender == self.user_id:
            return

        print(f'Processing event {event} in room {room.room_id}')

        if self.call_plugin(loop, event, room):
            return

        def reply(message: str):
            loop.create_task(self.client.room_send(
                                 room_id=room.room_id,
                                 message_type='m.room.message',
                                 content= {'body': message, 'msgtype': 'm.text'},
                                 ignore_unverified_devices=True))

        self.on_room_message(reply, event.formatted_body or event.body, event.sender)

    async def on_room_invite(self, room: MatrixRoom, event: InviteEvent):
        print(f'Auto-joining room {room.room_id}')
        await self.client.join(room.room_id)

    async def on_message_react(self, room: MatrixRoom, event: UnknownEvent):
        if event.type != 'm.reaction':
            return

        thread = Thread(target=self.call_plugin, args=(asyncio.get_event_loop(), event, room,))
        thread.daemon = True
        thread.start()

    async def run(self):
        print('Starting sync loop')
        await self.client.sync_forever(full_state=True, timeout=300000, loop_sleep_time=1000)

    def should_reply(self, message: str) -> bool:
        return any(e in message.lower() for e in [self.user_name, '@room'] + self.extra_triggers)

    def get_message(self) -> str:
        if not self.message_queue:
            self.message_queue = load_messages(self.db)
            random.shuffle(self.message_queue)

        message = self.message_queue[0]
        self.message_queue.pop(0)

        return message

    def store_new_message(self, message: str):
        all_messages = load_messages(self.db)
        if message in all_messages:
            return 'I already know that. Stop repeating yourself.'

        with open(self.db, 'w') as db:
            db.write('\n'.join(all_messages + [message]))

        return 'Sucessfully learnt (burp)'

    def learn_message(self, content: str):
        if not content.lower().startswith('learn'):
            return

        content = content[len(LEARN_PREFIX):].strip()

        if len(content) > MAX_MSG_LEN:
            return "Can't learn message: exceeds max size ({} bytes)".format(MAX_MSG_LEN)

        if content.startswith('/'):
            return "Can't learn message: starts with '/'"

        return self.store_new_message(content)


    def on_room_message(self, reply, content: str, sender: str):
        learnt = self.learn_message(content.strip())
        if learnt:
            reply(learnt)
            return

        if not self.should_reply(content):
            return

        reply(self.get_message().replace('!user', sender.split(':')[0]))

if __name__ == '__main__':
    main()
