# simple-matrix-bot

A simple python matrix bot implementation in python

## Installation

```bash
# Clone repository
$ git clone https://bitbucket.org/_Blue/simple-matrix-bot
$ cd simple-matrix-bot

# Install dependencies
$ pip install -r requirements.txt
```

## Deployment

```
$ ./bot.py https://my-matrix-endpoint '@my-user-name:my-matrix-endpoint' 'my-api-token' messages-file
```
